import os

from dotenv import load_dotenv


class Config:
    load_dotenv()
    URL = os.environ.get("ENV_URL")
    USR = os.environ.get("ENV_USR")
    PWD = os.environ.get("ENV_PWD")
